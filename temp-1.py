"""

    Copyright by Paul | 2020

    Examples:
    ------------------------------

    F = C * 1.8 + 32
    C = (F - 32) / 1.8
    K = C + 273.15

    ------------------------------

    1. C -> F
    2. F -> C
    3. F -> K
    4. C -> K
    5. K -> C
    6. K -> F
"""

# Integer to count through the list
counter = 0

# All example formulas and their equation
examples = [["C -> F", "{} * 1.8 + 32", "°F"], ["F -> C", "({} - 32) / 1.8", "°C"], ["C -> K", "{} + 273.15", "K"],
            ["F -> K", "(({} - 32) / 1.8) + 273.15", "K"], ["K -> C", "{} - 273.15", "°C"],
            ["K -> F", "({} - 273.15) * 1.8 + 32", "°F"]]

# Asks the user for a formula
print("Which conversion?\n")


# Function to check if a String is a Number
def is_number(s):
    try:
        float(s.replace(",", "."))
        return True
    except ValueError:
        return False


# Infinite loop until one formula is selected
while True:

    # Printing the current selected formula and checking if one is selected
    print(examples[counter % len(examples)][0])
    # User Input
    i = input("")
    if is_number(i):
        # Converting the example formula into a funtion and printing the output
        print(round(eval(examples[counter % len(examples)][1].format(i.replace(",", "."))), 2),
              examples[counter % len(examples)][2])
        break

    counter += 1
